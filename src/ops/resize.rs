use crate::ops::filter_type;
use crate::DynamicImage;
use mlua::{AnyUserData, Error, Lua};

pub(super) fn handle(
    _lua: &Lua,
    (ud, nwidth, nheight, filter_string): (AnyUserData, u32, u32, String),
) -> Result<DynamicImage, Error> {
    let image = ud.borrow::<DynamicImage>()?;
    let filter = filter_type::parse(filter_string.as_str())?;
    let resized = image::imageops::resize(&image.delegate, nwidth, nheight, filter);
    Ok(DynamicImage {
        delegate: image::DynamicImage::from(resized),
    })
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn resize() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local image, imageops = require('image'), require('image.ops')
            local img = image.open('testdata/fractal.png')
            local resized = imageops.resize(img, 224, 224, 'triangle')
            return resized:height(), resized:width()
        "#;
        let (height, width): (u32, u32) = lua.load(script).eval()?;
        assert_eq!(height, 224);
        assert_eq!(width, 224);
        Ok(())
    }
}
