use crate::DynamicImage;
use mlua::{AnyUserData, Error, Lua};

pub(super) fn handle(_lua: &Lua, ud: AnyUserData) -> Result<(), Error> {
    let mut image = ud.borrow_mut::<DynamicImage>()?;
    image::imageops::flip_horizontal_in_place(&mut image.delegate);
    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn flip_horizontal_in_place() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local image, imageops = require('image'), require('image.ops')
            local img = image.open('testdata/fractal.png')
            imageops.flip_horizontal_in_place(img)
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
