use crate::DynamicImage;
use mlua::{AnyUserData, Error, Lua};

pub(super) fn handle(_lua: &Lua, (ud, sigma): (AnyUserData, f32)) -> Result<DynamicImage, Error> {
    let image = ud.borrow::<DynamicImage>()?;
    let blurred = image::imageops::blur(&image.delegate, sigma);
    Ok(DynamicImage {
        delegate: image::DynamicImage::from(blurred),
    })
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn blur() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local image, imageops = require('image'), require('image.ops')
            local img = image.open('testdata/fractal.png')
            imageops.blur(img, 25)
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
