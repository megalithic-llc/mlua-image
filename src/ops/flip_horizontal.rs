use crate::DynamicImage;
use mlua::{AnyUserData, Error, Lua};

pub(super) fn handle(_lua: &Lua, ud: AnyUserData) -> Result<DynamicImage, Error> {
    let image = ud.borrow::<DynamicImage>()?;
    let flipped = image::imageops::flip_horizontal(&image.delegate);
    Ok(DynamicImage {
        delegate: image::DynamicImage::from(flipped),
    })
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn flip_horizontal() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local image, imageops = require('image'), require('image.ops')
            local img = image.open('testdata/fractal.png')
            imageops.flip_horizontal(img)
        "#;
        lua.load(script).exec()?;
        Ok(())
    }
}
