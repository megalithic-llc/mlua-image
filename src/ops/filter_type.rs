use image::imageops::FilterType;
use mlua::Error;

pub(crate) fn parse(s: &str) -> Result<FilterType, Error> {
    let filter = match s {
        "CatmullRom" | "catmullrom" => FilterType::CatmullRom,
        "Gaussian" | "gaussian" => FilterType::Gaussian,
        "Lanczos3" | "lanczos3" => FilterType::Lanczos3,
        "Nearest" | "nearest" => FilterType::Nearest,
        "Triangle" | "triangle" => FilterType::Triangle,
        _ => return Err(Error::RuntimeError("Unsupported filter".to_string())),
    };
    Ok(filter)
}

#[cfg(test)]
mod tests {
    use image::imageops::FilterType;
    use std::error::Error;

    #[test]
    fn parse_valid() -> Result<(), Box<dyn Error>> {
        use super::parse;
        assert_eq!(parse("CatmullRom")?, FilterType::CatmullRom);
        assert_eq!(parse("catmullrom")?, FilterType::CatmullRom);
        assert_eq!(parse("Gaussian")?, FilterType::Gaussian);
        assert_eq!(parse("gaussian")?, FilterType::Gaussian);
        assert_eq!(parse("Lanczos3")?, FilterType::Lanczos3);
        assert_eq!(parse("lanczos3")?, FilterType::Lanczos3);
        assert_eq!(parse("Nearest")?, FilterType::Nearest);
        assert_eq!(parse("nearest")?, FilterType::Nearest);
        assert_eq!(parse("Triangle")?, FilterType::Triangle);
        assert_eq!(parse("triangle")?, FilterType::Triangle);
        Ok(())
    }

    #[test]
    fn parse_invalid() -> Result<(), Box<dyn Error>> {
        use super::parse;
        assert!(parse("").is_err());
        assert!(parse("abc").is_err());
        Ok(())
    }
}
