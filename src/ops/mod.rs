mod blur;
pub(crate) mod filter_type;
mod flip_horizontal;
mod flip_horizontal_in_place;
mod flip_vertical;
mod flip_vertical_in_place;
mod resize;
mod rotate180;
mod rotate270;
mod rotate90;

use mlua::{Error, Lua, Table};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let module = lua.create_table()?;
    module.set("blur", lua.create_function(blur::handle)?)?;
    module.set("flip_horizontal", lua.create_function(flip_horizontal::handle)?)?;
    module.set(
        "flip_horizontal_in_place",
        lua.create_function(flip_horizontal_in_place::handle)?,
    )?;
    module.set("flip_vertical", lua.create_function(flip_vertical::handle)?)?;
    module.set(
        "flip_vertical_in_place",
        lua.create_function(flip_vertical_in_place::handle)?,
    )?;
    module.set("resize", lua.create_function(resize::handle)?)?;
    module.set("rotate180", lua.create_function(rotate180::handle)?)?;
    module.set("rotate270", lua.create_function(rotate270::handle)?)?;
    module.set("rotate90", lua.create_function(rotate90::handle)?)?;

    // Preload module
    let globals = lua.globals();
    let package = globals.get::<_, Table>("package")?;
    let loaded = package.get::<_, Table>("loaded")?;
    loaded.set("image.ops", module)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        super::preload(&lua)?;
        let _module: Table = lua.load("return require('image.ops')").eval()?;
        Ok(())
    }
}
