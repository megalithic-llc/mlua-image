use crate::DynamicImage;
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, path: String) -> Result<DynamicImage, Error> {
    let dynamic_image = image::open(path).map_err(|err| Error::RuntimeError(err.to_string()))?;
    let user_data = DynamicImage {
        delegate: dynamic_image,
    };
    Ok(user_data)
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn open_non_existent() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local image = require('image')
            image.open('/tmp/_non_existent_')
        "#;
        let result = lua.load(script).exec();
        assert!(result.is_err());
        Ok(())
    }

    #[test]
    fn open_fractal_png() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
            local image = require('image')
            image.open('testdata/fractal.png')
        "#;
        let result = lua.load(script).exec();
        assert!(result.is_ok());
        Ok(())
    }
}
