mod dynamic_image;
mod open;
mod ops;
mod pixel;

pub use dynamic_image::DynamicImage;
use mlua::{Error, Lua, Table};

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Preload submodules
    ops::preload(lua)?;

    // Configure module table
    let table = lua.create_table()?;
    table.raw_set("open", lua.create_function(open::handle)?)?;

    // Preload module
    let globals = lua.globals();
    let package = globals.get::<_, Table>("package")?;
    let loaded = package.get::<_, Table>("loaded")?;
    loaded.set("image", table)?;
    Ok(())
}
