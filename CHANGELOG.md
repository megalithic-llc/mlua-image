# mlua-image Changelog

## [Unreleased]
### Changed
- [#21](https://gitlab.com/megalithic-llc/mlua-image/-/issues/21) Upgrade image from 0.24.9 → 0.25.0

## [0.1.3] - 2024-03-02
### Changed
- [#20](https://gitlab.com/megalithic-llc/mlua-image/-/issues/20) Upgrade 2 crates
- [#18](https://gitlab.com/megalithic-llc/mlua-image/-/issues/18) Upgrade Rust from 1.75.0 → 1.76.0

## [0.1.2] - 2024-02-03
### Changed
- [#13](https://gitlab.com/megalithic-llc/mlua-image/-/issues/13) Upgrade Rust from 1.74.1 → 1.75.0

### Fixed
- [#17](https://gitlab.com/megalithic-llc/mlua-image/-/issues/17) DynamicImage get_pixel() fails to subtract 1 from 1-based Lua indices
- [#12](https://gitlab.com/megalithic-llc/mlua-image/-/issues/12) DynamicImage mutex is unnecessary to mutate
- [#11](https://gitlab.com/megalithic-llc/mlua-image/-/issues/11) Cannot rotate an image twice due to CallbackDestructed

## [0.1.1] - 2024-01-13
### Added
- [#9](https://gitlab.com/megalithic-llc/mlua-image/-/issues/9) Support imageops: blur, flip_horizontal, flip_horizontal_in_place, flip_vertical, flip_vertical_in_place, rotate90, rotate180, rotate270
- [#7](https://gitlab.com/megalithic-llc/mlua-image/-/issues/7) Support remaining DynamicImage fns
- [#6](https://gitlab.com/megalithic-llc/mlua-image/-/issues/6) Support imageops resize()

### Changed
- [#10](https://gitlab.com/megalithic-llc/mlua-image/-/issues/10) Upgrade mlua from 0.9.2 → 0.9.4

## [0.1.0] - 2024-01-07
### Added
- [#5](https://gitlab.com/megalithic-llc/mlua-image/-/issues/5) Support DynamicImage save() and save_with_format()
- [#4](https://gitlab.com/megalithic-llc/mlua-image/-/issues/4) Support DynamicImage height(), width(), and into_bytes() accessors
- [#3](https://gitlab.com/megalithic-llc/mlua-image/-/issues/3) Support DynamicImage to_rgb8() and all other `to_` fns
- [#2](https://gitlab.com/megalithic-llc/mlua-image/-/issues/2) Support opening an image
- [#1](https://gitlab.com/megalithic-llc/mlua-image/-/issues/1) Put CI+CD in place
