# mlua-image

Lua bindings for the [image](https://crates.io/crates/image) imaging library.

[![License](http://img.shields.io/badge/Licence-MIT-blue.svg)](LICENSE)
[![Arch](https://img.shields.io/badge/Arch-aarch64%20|%20amd64%20|%20armv7-blue.svg)]()
[![Lua](https://img.shields.io/badge/Lua-5.1%20|%205.2%20|%205.3%20|%205.4%20|%20LuaJIT%20|%20LuaJIT%205.2-blue.svg)]()

## Installing

Add to your Rust project using one of MLua's features: [lua51, lua52, lua53, lua54, luajit, luajit52].

```shell
$ cargo add mlua-image --features luajit
```

## Using

```rust
use mlua::Lua;
use mlua_image;

let lua = Lua::new();
mlua_image::preload(&lua)?;
let script = r#"
    local image = require('image')
    local img = image.open('testdata/fractal.png')
    img:save_with_format('fractal.gif', 'gif')
    return img:height(), img:width()
"#;
let (height, width): (u32, u32) = lua.load(script).eval()?; // returns: (800, 800)
```

## Testing

```shell
$ make check
```
